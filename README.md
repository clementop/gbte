# Gameboy Tileset Editor

Available at https://clementop.gitlab.io/gbte/.

A web-based tile and tileset editor for gameboy chr files.
Upload an existing tileset or create your own from scratch.
While this editor is based on web technologies, it does not require a server and
can be run locally by simply opening the `index.html` file directly.

## Tileset Functions

### Load
Load an existing tileset by choosing a file from your local filesystem. The editor will be completely refreshed to include your chosen tileset.

### Save
Save your work by clicking on the `Download` button. The default filename is `tileset.chr`. Override the filename using the accompanying input. The `.chr` suffix is automatically added.

### Change Tile
Change the working tile by selecting a tile within the larger tileset view. The tileset provides a hover indicator over the tile you are about to select.

## Tile Functions

### Edit Pixel
Change a pixel by simply clicking in the larger tile view. The tile view provides a hover indicator over the tile you are about to select.

### Pixel Modes
Different modes are available for manipulating the pixels. The current selected mode can be updated using the keyboard number keys 1-6.

1. **Set White**  
  Set the pixel to the lightest foreground color.
2. **Set Light**  
  Set the pixel to the light color.
3. **Set Dark**  
  Set the pixel to the dark color.
4. **Set Black**  
  Set the pixel to the darkest background color.
5. **Rotate Darker**  
  Set the pixel to the next darker color.
6. **Rotate Lighter (Default)**  
  Set the pixel to the next lighter color.
