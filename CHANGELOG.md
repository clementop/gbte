# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.3] - 2021-01-02
### Fixed
- Button toolbar takes full window width.

### Changed
- Using Bootstrap 5.0 stylesheet
- Upload and download buttons moved to top nav

## [0.0.2] - 2020-04-18
### Added
- Project is available publicly at https://clementop.gitlab.io/gbte/.
- Button toolbar for choosing between brush modes.

### Fixed
- Hovering over 2x tileset viewer displays tile hover indicator.

## [0.0.1] - 2020-01-01
### Added
- 1x and 2x tileset viewers.
- 4x, 16x, and 32x tile viewers.
- Change selected tile via the 2x tileset viewer.
- Change tile pixels via the 32x tile viewer.
- Choose between 6 pixel edit modes using keys 1-6.
- Reverse pixel edit mode by holding the shift key while clicking tile pixel.
- Download tileset with an optional name.
- Upload an exiting tileset.

[Unreleased]: https://gitlab.com/clementop/gbte/compare/v0.0.3...master
[0.0.3]: https://gitlab.com/clementop/gbte/compare/v0.0.2...v0.0.3
[0.0.2]: https://gitlab.com/clementop/gbte/compare/v0.0.1...v0.0.2
[0.0.1]: https://gitlab.com/clementop/gbte/-/tags/v0.0.1
