// var ENABLE_PERF_TIMING = true;
var ENABLE_PERF_TIMING = false;

var $ = {
    new: function (node) { return document.createElement(node); },
    q: function(query) { return document.querySelectorAll(query); },
    q1: function(query) { return document.querySelector(query); },
    id: function(id) { return document.getElementById(id); },
};

var hex = "0123456789ABCDEF";

var COLOR_WHITE = 0;
var COLOR_LIGHT = 1;
var COLOR_DARK  = 2;
var COLOR_BLACK = 3;

var COLOR_MODES = [
    function setWhite(currentValue, shiftEnabled) {
        return shiftEnabled ? COLOR_BLACK : COLOR_WHITE;
    },
    function setLight(currentValue, shiftEnabled) {
        return shiftEnabled ? COLOR_DARK : COLOR_LIGHT;
    },
    function setDark(currentValue, shiftEnabled)  {
        return shiftEnabled ? COLOR_LIGHT : COLOR_DARK;
    },
    function setBlack(currentValue, shiftEnabled) {
        return shiftEnabled ? COLOR_WHITE : COLOR_BLACK;
    },
    function rotateAsc(currentValue, shiftEnabled)   {
        return (currentValue + (shiftEnabled ? 3 : 1)) % 4;
    },
    function rotateDesc(currentValue, shiftEnabled)   {
        return (currentValue + (shiftEnabled ? 1 : 3)) % 4;
    },
];

var BUTTONS = [
    [COLOR_WHITE],
    [COLOR_LIGHT],
    [COLOR_DARK],
    [COLOR_BLACK],
    [COLOR_WHITE,COLOR_LIGHT,COLOR_DARK,COLOR_BLACK],
    [COLOR_BLACK,COLOR_DARK,COLOR_LIGHT,COLOR_WHITE],
];

var CURRENT_TILE = "00";
var CURRENT_COLOR_MODE_ID = 5;
var CURRENT_COLOR_MODE = COLOR_MODES[CURRENT_COLOR_MODE_ID];
var TILESET_BYTES = new Uint8Array(4096);

var CANVAS = [];
var COLORS = [
    "#9bbc0f",
    "#8bac0f",
    "#306230",
    "#0f380f",
];
var COLORS_RGBA = [
    [155, 188, 15, 255],
    [139, 172, 15, 255],
    [48, 98, 48, 255],
    [15, 56, 15, 255],
];

function buildChrRow (rowHex, enableEvents) {
    var $row = $.new("tr");
    $rowHex = $.new("th");
    $rowHex.innerText = rowHex;
    $row.appendChild($rowHex);
    for (var i = 0; i < hex.length; i++) {
        var colHex = hex[i];
        var $tile = $.new("td");
        $tile.setAttribute("tile", "" + rowHex + colHex);
        if (enableEvents) {
            listen($tile, "click", onClickTile.bind(this, $tile));
        }
        $row.appendChild($tile)

    }
    return $row
}

function onClickTile ($tile, ev) {
    var tile = $tile.getAttribute("tile");
    setCurrentTile(tile);
}

function setCurrentTile(tile) {
    CURRENT_TILE = tile;
    drawTileTables();
}

function buildChrTable (targetId, size, enableEvents, className) {
    var $chr = $.new("div");
    $chr.id = targetId;
    $chr.classList.add("chr-wrapper", "chr-" + size + "x", className);

    var $table = $.new("table");
    $table.classList.add("chr", "chr-" + size + "x");
    if (enableEvents) {
        $table.classList.add("active")
    }
    var $hr = $.new("tr");
    $hr.appendChild($.new("th"));
    for (var i = 0; i < hex.length; i++) {
        var colHex = hex[i];
        var $tile = $.new("th");
        $tile.innerText = colHex;
        $hr.appendChild($tile);
    }
    $table.appendChild($hr);

    for (var i = 0; i < hex.length; i++) {
        var rowHex = hex[i];
        $table.appendChild(buildChrRow(rowHex, enableEvents));
    }

    var $canvas = $.new("canvas");
    var dimension = size * 8 * 16;
    $canvas.setAttribute("height", dimension);
    $canvas.setAttribute("width", dimension);
    CANVAS.push({
        size: size,
        ctx: $canvas.getContext("2d"),
        width: dimension,
        height: dimension
    });

    $chr.appendChild($table);
    $chr.appendChild($canvas);
    $.id(targetId).replaceWith($chr);
}

function getTileBytes (tile) {
    var tileBytes = new Uint8Array(16);
    var offsetTile = parseInt(tile, 16) * 16; // Each tile is 16 bytes
    for (var i = 0; i < 16; i++) {
        tileBytes[i] = TILESET_BYTES[offsetTile + i];
    }
    return tileBytes;
}

function toPixelValues(bytes) {
    var pixelValues = [];
    for (var i = 0; i < bytes.length; i+=2) {
        var lsbyte = bytes[i];
        var msbyte = bytes[i + 1];
        for (var j = 0; j < 8; j++) {
            pixelValues.push(getPixelBits(lsbyte, msbyte, 7 - j));
        }
    }
    return pixelValues;
}

function buildBrushButton (id, key, colors) {
    var $row = $.new("tr");

    for (var i = 0; i < colors.length; i++) {
        var color = colors[i];
        var $pixel = $.new("td");
        $pixel.setAttribute("value", color);
        $row.appendChild($pixel);
    }

    var $tile = $.new("table");
    $tile.classList.add("tile", "tile-16x");
    $tile.appendChild($row);

    var $button = $.new("button");
    $button.classList.add("brush", "btn", "btn-outline-secondary");
    if (id === CURRENT_COLOR_MODE_ID) {
        $button.classList.add("active");
    }
    $button.innerText = key;
    $button.appendChild($tile);
    $button.setAttribute("brush", id);
    $button.setAttribute("type", "button");
    listen($button, "click", onClickBrushButton.bind(this, $button));
    return $button;
}

function onClickBrushButton ($button, ev) {
    var mode = $button.getAttribute("brush");
    setColorMode(mode);
}

function drawBrushButtons () {
    var $brushes = $.id("brushes");
    if ($brushes.children.length == 0) {
    for (var i = 0; i < BUTTONS.length; i++) {
        var $button = buildBrushButton(i, i + 1, BUTTONS[i]);
        $brushes.appendChild($button);
    }
}
}

function drawTileTables () {
    var tileValues = toPixelValues(getTileBytes(CURRENT_TILE));
    buildTileTable("tile-4x", 4, CURRENT_TILE, tileValues, false, "mt-2");
    buildTileTable("tile-16x", 16, CURRENT_TILE, tileValues, false, "mt-2");
    buildTileTable("tile-32x", 32, CURRENT_TILE, tileValues, true);
}

function buildTileTable (targetId, size, tile, tileValues, enableEvents, className) {
    var $table = $.new("table");
    $table.id = targetId;
    $table.classList.add("tile", "tile-" + size + "x", className);
    if (enableEvents) {
        $table.classList.add("active")
    }
    $table.setAttribute("tile", tile);

    var valueIndex = 0;
    for (var i = 0; i < 8; i++) {
        var $row = $.new("tr");
        for (var j = 0; j < 8; j++) {
            var $pixel = $.new("td");
            var pixel = "" + i + j;
            $pixel.setAttribute("pixel", pixel);
            $pixel.setAttribute("value", tileValues[valueIndex++]);
            if (enableEvents) {
                listen($pixel, "click", onClickTileCell.bind(this, $pixel));
            }
            $row.appendChild($pixel);
        }
        $table.appendChild($row);
    }

    $.id(targetId).replaceWith($table);
}

function setColorMode(mode) {
    if (mode >= 0 && mode < COLOR_MODES.length) {
        var $prevButton = $.q1("button.brush[brush='" + CURRENT_COLOR_MODE_ID + "']");
        $prevButton.classList.remove("active");
        CURRENT_COLOR_MODE_ID = mode;
        var $nextButton = $.q1("button.brush[brush='" + CURRENT_COLOR_MODE_ID + "']");
        $nextButton.classList.add("active");
        CURRENT_COLOR_MODE = COLOR_MODES[mode];
    }
}

function onKeyDown(ev) {
    // Numbers are mapped to setting color codes
    var num = parseInt(ev.key);
    if (num > 0 && num <= COLOR_MODES.length) {
        setColorMode(num - 1);
    }
}

function getPixelValue($pixel) {
    return parseInt($pixel.getAttribute("value")) || 0;
}

function onClickTileCell($pixel, ev) {
    var pixel = $pixel.getAttribute("pixel");
    var value = getPixelValue($pixel);
    var $pixels = $.q("[pixel='" + pixel + "']");
    var shiftEnabled = ev.shiftKey;
    var newValue = CURRENT_COLOR_MODE(value, shiftEnabled);
    $pixels.forEach(setAttr.bind(this, "value", newValue));
    setPixelBits(CURRENT_TILE, pixel, newValue);
    drawCanvasPixel(CURRENT_TILE, pixel, newValue);
}

function drawCanvasPixel(tile, pixel, value) {
    var coord = getPixelCoords(tile, pixel);
    var color = COLORS[value];
    CANVAS.forEach(function (canvas) {
        var s = canvas.size;
        canvas.ctx.fillStyle = color;
        canvas.ctx.fillRect(coord[0] * s, coord[1] * s, s, s);
    });
}

function getPixelCoords(tile, pixel) {
    var tileNum = parseInt(tile, 16);
    var pixelNum = parseInt(pixel, 8);
    var x = ((tileNum & 15) * 8) + (pixelNum & 7);
    var y = (((tileNum >>> 4) & 15) * 8) + ((pixelNum >>> 3) & 7);
    return [x, y]
}

function msb(radix, value) {
    var bitnum = Math.log2(radix);
    var mask = Math.pow(2, bitnum) - 1;
    return (parseInt(value, radix) >>> bitnum) & mask;
}

var msb8 = msb.bind(this, 8);
var msb16 = msb.bind(this, 16);

function lsb(radix, value) {
    return parseInt(value, radix) & (radix - 1);
}

var lsb8 = lsb.bind(this, 8);
var lsb16 = lsb.bind(this, 16);

function getPixelBits(lsbyte, msbyte, bitnum) {
    var msbit = ((msbyte >>> bitnum) & 1);
    var lsbit = ((lsbyte >>> bitnum) & 1);
    return (msbit << 1) | lsbit;
}

function setPixelBits(tile, pixel, value) {
    var offsetTile = parseInt(tile, 16) * 16;
    var offsetRow = msb8(pixel); // Row being updated
    var bitnum = 7 - lsb8(pixel); // Col being updated

    var byteOffset = offsetTile + (offsetRow * 2); // two bytes per row
    var lsbyte = TILESET_BYTES[byteOffset];
    var msbyte = TILESET_BYTES[byteOffset + 1];

    var lsbit = value & 1;
    var msbit = (value >>> 1) & 1;

    var mask = ~(1 << bitnum)
    var newlsbyte = (lsbyte & mask) | (lsbit << bitnum);
    var newmsbyte = (msbyte & mask) | (msbit << bitnum);

    TILESET_BYTES[byteOffset] = newlsbyte;
    TILESET_BYTES[byteOffset + 1] = newmsbyte;
}

function moveTile(source, target) {
    var bytes = getTileBytes(source);
    var targetOffset = parseInt(target, 16) * 16; // Each tile is 16 bytes
    for (var i = 0; i < 16; i++) {
        TILESET_BYTES[targetOffset++] = bytes[i];
    }

    renderCanvases();
    setCurrentTile(target);
}

function onClickDownload(ev) {
    var $filename = $.id("filename");
    var filename = ($filename.value || "tileset") + ".chr";

    var $download = $.new("a");
    $download.style = "display: none";
    var blob = new Blob([TILESET_BYTES], {type: "octet/stream"});
    var url = window.URL.createObjectURL(blob);
    $download.href = url;
    $download.download = filename;
    document.body.appendChild($download);
    $download.click();
    window.URL.revokeObjectURL(url);
}

function onClickUpload(ev) {
    $.id("upload").click();
}

function onChangeFile(ev) {
    ev.preventDefault();

    var files = (event.dataTransfter ? event.dataTransfter : event.target).files;
    if (files.length) {
        var file = files[0];
        self.loadFile(file);
    }
    return false;
}

function loadFile(file) {
    var reader = new FileReader();
    reader.onload = function () {
        TILESET_BYTES = new Uint8Array(this.result);
        render();
        renderCanvases();
        $.id("upload").value = "";
    }
    reader.readAsArrayBuffer(file);
}

function setAttr (key, value, $el) { $el.setAttribute(key, value); }

function ready(fn) {
  if (document.readyState != 'loading'){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

function listen (target, event, callback) {
    target.addEventListener(event, callback);
}

function renderCanvases () {
    CANVAS.forEach(time.bind(this, "canvas render", renderCanvas));
}

function time (desc, func) {
    var start = new Date().getTime();

    var args = Array.prototype.slice.call(arguments, 2);
    func.apply(this, args);

    if (ENABLE_PERF_TIMING) {
        var end = new Date().getTime();
        var ms = end - start;
        var s = parseFloat(ms / 1000).toFixed(1);
        console.log("" + desc + ": " + ms + "ms " + s + "s");
    }
}

function renderCanvas(canvas) {
    var lsbyte = null;
    var msbyte = null;
    var color = null;

    var pixels = canvas.ctx.getImageData(0, 0, canvas.width, canvas.height);

    var w = h = canvas.size;
    var pixelSize = w * h;
    var rowSize = w * 16 * 8; // (16 tiles) * (8 pixels)
    var tileOffset = 0;
    var dataOffset = 0;
    var pixelOffset = 0;
    for (var ty = 0; ty < 16; ty++) {
        for (var tx = 0; tx < 16; tx++) {
            for (var tr = 0; tr < 8; tr++) {
                dataOffset = calculatePixelOffset(w, h, ty, tx, tr);
                lsbyte = TILESET_BYTES[tileOffset++];
                msbyte = TILESET_BYTES[tileOffset++];
                for (var tc = 7; tc >= 0; tc--) {
                    pixelOffset = dataOffset + ((7 - tc) * 4 * w);
                    color = COLORS_RGBA[getPixelBits(lsbyte, msbyte, tc)];
                    for (var pr = 0; pr < h; pr++) {
                        for (var pc = 0; pc < w; pc++) {
                            pixels.data[pixelOffset++] = color[0];
                            pixels.data[pixelOffset++] = color[1];
                            pixels.data[pixelOffset++] = color[2];
                            pixels.data[pixelOffset++] = color[3];
                        }
                        pixelOffset += (rowSize - w) * 4; // next line
                    }
                }
            }
        }
    }

    canvas.ctx.putImageData(pixels, 0, 0);
}

function calculatePixelOffset(w, h, ty, tx, tr) {
    var rowSize = w * 16 * 8;
    return 4 * ((rowSize * ty * 8 * h) + (rowSize * tr * h) + (w * tx * 8));
}

function render () {
    CURRENT_TILE = "00";
    CURRENT_COLOR_MODE_ID = 5;
    CURRENT_COLOR_MODE = COLOR_MODES[CURRENT_COLOR_MODE_ID];
    CANVAS = [];

    buildChrTable("chr-1x", 1, false, "mt-2");
    buildChrTable("chr-2x", 2, true);

    drawTileTables();

    drawBrushButtons();
}

ready(function () {
    render();
    renderCanvases();

    listen(window, "keydown", onKeyDown);
    listen($.id("download"), "click", onClickDownload);
    listen($.id("upload-btn"), "click", onClickUpload);
    listen($.id("upload"), "change", onChangeFile);
});
